// # FSM Simulation

// edges = {(1, 'a') : 2,
//          (2, 'a') : 2,
//          (2, '1') : 3,
//          (3, '1') : 3}

// accepting = [3]

// def fsmsim(string, current, edges, accepting):
//     if string == "":
//         print current in accepting
//         return current in accepting
//     else:
//         letter = string[0]
//         if (current, letter) in edges:
//             nc = edges[(current, letter)]
//             print letter, string[1:], nc
//             return fsmsim(string[1:], nc, edges, accepting)
//         else:
//             return False

// print fsmsim("aaa111",1,edges,accepting)
// # >>> True

var edges = {
  1: {
    a: 2
  },
  2: {
    a: 2,
    1: 3,
  },
  3: {1: 3}
};

var accepting = [3];

var fsmsim = function(string, current, edges, accepting) {
  if (string === '') {
    return accepting.indexOf(current) >= 0;
  } else {
    var letter = string[0];
    if (edges[current] && edges[current][letter]) {
      nc = edges[current][letter];
      return fsmsim(string.substr(1), nc, edges, accepting);
    } else {
      return false;
    }
  }
};

console.log(fsmsim('aaa111', 1, edges, accepting));
