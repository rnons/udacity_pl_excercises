var edges = {
  1: {
    a: [2, 3]
  },
  2: {
    a: [2]
  },
  3: {
    b: [4, 3]
  },
  4: {
    c: [5]
  }
};
var accepting = [2, 5];

var nfsmsim = function(string, current, edges, accepting) {
    if (string == "") {
        return accepting.indexOf(current) >= 0;
    } else {
        var letter = string[0];
        if (edges[current] && edges[current][letter]) {
          var dest = edges[current][letter];
          var flag = false;
          for (d in dest) {
              flag = flag || nfsmsim(string.substr(1), dest[d], edges, accepting);
          }
          return flag;
        } else {
          return false;
        }
    }
};


console.log("Test case 1 passed: " + (nfsmsim("abc", 1, edges, accepting) === true));
console.log("Test case 2 passed: " + (nfsmsim("aaa", 1, edges, accepting) === true));
console.log("Test case 3 passed: " + (nfsmsim("abbbc", 1, edges, accepting) === true));
console.log("Test case 4 passed: " + (nfsmsim("aabc", 1, edges, accepting) === false));
console.log("Test case 5 passed: " + (nfsmsim("", 1, edges, accepting) === false));
