-- Problem set 1-6

module NfsmAccepts where

import Data.Array (elemIndex, map, concatMap)
import Data.Map (Map(..), fromList, lookup, keys, toList)
import Data.Maybe (Maybe(..))
import Data.String (take, drop)
import Data.Tuple (Tuple(..))
import Debug.Trace (print)

type NFSM = Map (Tuple Number String) Number

edges :: NFSM
edges = fromList 
  [ Tuple (Tuple 1 "a") [2, 3]
  , Tuple (Tuple 2 "a") [2]
  , Tuple (Tuple 3 "b") [4, 2]
  , Tuple (Tuple 4 "c") [5]
  ]

accepting :: [Number]
accepting = [5]

edges2 :: NFSM
edges2 = fromList
  [ Tuple (Tuple 1 "a") [1]
  , Tuple (Tuple 2 "a") [2]
  ]

accepting2 :: [Number]
accepting2 = [2]

nfsmAccepts :: Number -> NFSM -> [Number] -> {} -> Maybe String
nfsmAccepts current fsm done visited =
  -- case lookup current done of
  if elemIndex current done >= 0 then
    map (\(Tuple (Tuple c e) v) ->
      if c == current then
        concatMap (\v' -> nfsmAccepts v' fsm done visited) v
      else
        Nothing
      ) (toList fsm)
  else Just ""

main = do
  print $ "Test 1: " ++
    show (nfsmAccepts 1 edges accepting {})
