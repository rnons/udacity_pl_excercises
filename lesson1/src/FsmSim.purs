module FsmSim where

import Data.Array (elemIndex)
import Data.Map (Map(..), fromList, lookup)
import Data.Maybe (Maybe(..))
import Data.String (take, drop)
import Data.Tuple (Tuple(..))
import Debug.Trace (print)

type FSM = Map (Tuple Number String) Number

edges :: FSM
edges = fromList 
  [ Tuple (Tuple 1 "a") 2
  , Tuple (Tuple 2 "a") 2
  , Tuple (Tuple 2 "1") 3
  , Tuple (Tuple 3 "1") 3
  ]

accepting :: [Number]
accepting = [3]

fsmSim :: String -> Number -> FSM -> [Number] -> Boolean
fsmSim "" current _ done = elemIndex current done >= 0
fsmSim str current fsm done = do
  case lookup (Tuple current x) fsm of
    Just v -> fsmSim xs v fsm done
    Nothing -> false
  where
    x = take 1 str
    xs = drop 1 str

main = print $ fsmSim "aaa111" 1 edges accepting
