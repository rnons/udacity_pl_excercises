module MyFirst where

import Data.Array ((!!))
import Data.String (split)
import Debug.Trace (print)

myFirstYourSecond :: String -> String -> Boolean
myFirstYourSecond p q = do
    splitedP !! 0 == splitedQ !! 1
  where
    splitedP = split " " p
    splitedQ = split " " q

main = print $ myFirstYourSecond "bell hooks" "curer bell"
