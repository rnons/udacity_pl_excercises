module SumNums where

import Data.Array (map)
import Data.Foldable (foldl)
import Data.String.Regex (regex, match, parseFlags)
import Debug.Trace (print)
import Global

sumNums :: String -> Number
sumNums sentence = 
    foldl (+) 0 numbers 
  where
    exp = regex "\\d+" $ parseFlags "g"
    numbers = map (readInt 10) $ match exp sentence

main =
    if output == test_case_output then
      print "Test case passed."
    else do
      print "Test case failed."
      print output
  where
    test_case_input = "The Act of Independence of Lithuania was signed on February 16, 1918, by 20 council members."
    test_case_output = 1954
    output = sumNums test_case_input
