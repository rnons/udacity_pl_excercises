module NfsmSim where

import Data.Array (elemIndex, map)
import Data.Map (Map(..), fromList, lookup)
import Data.Maybe (Maybe(..))
import Data.String (take, drop)
import Data.Tuple (Tuple(..))
import Debug.Trace (print)

type NFSM = Map (Tuple Number String) [Number]

edges :: NFSM
edges = fromList 
  [ Tuple (Tuple 1 "a") [2, 3]
  , Tuple (Tuple 2 "a") [2]
  , Tuple (Tuple 3 "b") [4, 3]
  , Tuple (Tuple 4 "c") [5]
  ]

accepting :: [Number]
accepting = [2, 5]

nfsmSim :: String -> Number -> NFSM -> [Number] -> Boolean
nfsmSim "" current _ done = elemIndex current done >= 0
nfsmSim str current fsm done = do
  case lookup (Tuple current x) fsm of
    Just vs -> some $ map (\v -> nfsmSim xs v fsm done) vs
    Nothing -> false
  where
    x = take 1 str
    xs = drop 1 str
    some :: [Boolean] -> Boolean
    some bs = elemIndex true bs >= 0

main = do
  print $ "Test case 1 passed: " ++ 
    show (nfsmSim "abc" 1 edges accepting == true)
  print $ "Test case 2 passed: " ++ 
    show (nfsmSim "aaa" 1 edges accepting == true)
  print $ "Test case 3 passed: " ++ 
    show (nfsmSim "abbbc" 1 edges accepting == true)
  print $ "Test case 4 passed: " ++ 
    show (nfsmSim "aabc" 1 edges accepting == false)
  print $ "Test case 5 passed: " ++ 
    show (nfsmSim "" 1 edges accepting == false)
